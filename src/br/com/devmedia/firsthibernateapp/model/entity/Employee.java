package br.com.devmedia.firsthibernateapp.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Employees")
@PrimaryKeyJoinColumn(name = "user_id")
public class Employee extends User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column
	private String office;

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	@Override
	public String toString() {
		return super.toString() + " Employee [office=" + office + "]";
	}

}