package br.com.devmedia.firsthibernateapp.util;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class Em {

	private static EntityManager em = null;

	public static EntityManager getEm() {
		if (Em.em == null) {
			Em.em = Persistence.createEntityManagerFactory("FirstHibernateApp")
					.createEntityManager();
		}
		return Em.em;
	}

}