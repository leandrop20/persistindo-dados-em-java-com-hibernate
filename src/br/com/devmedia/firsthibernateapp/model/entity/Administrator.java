package br.com.devmedia.firsthibernateapp.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Administrators")
@PrimaryKeyJoinColumn(name = "user_id")
public class Administrator extends User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column
	private String sector;

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@Override
	public String toString() {
		return super.toString() + " Administrator [sector=" + sector + "]";
	}

}